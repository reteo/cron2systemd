"""Provides an interface for command-line arguments.

Copyright © 2024 Lampros Liontos

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see https://www.gnu.org/licenses/.
"""

import argparse


def parser():
    """Generates a command-line parser."""
    parser = argparse.ArgumentParser(
        prog="cron2systemd",
        description="Generates systemd files from a supplied crontab entry.",
    )

    parser.add_argument("crontab", help="The crontab line to use.")
    parser.add_argument("name", help="The name of the generated service.")
    parser.add_argument(
        "-d",
        "--description",
        help="The description for the generated service.",
        default="A converted cron job.",
    )
    parser.add_argument(
        "-l",
        "--logging",
        help="The logging level (default is info).",
        choices=["debug", "info", "warning", "error"],
        default="info",
    )

    return parser.parse_args()
