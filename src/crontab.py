"""Creates the Crontab object, which stores a crontab string, and can
generate service and timer strings from the crontab data.

Copyright © 2024 Lampros Liontos

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see https://www.gnu.org/licenses/.

"""

import re
import os
import textwrap


class Crontab:
    """Converts specified name and crontab to systemd service and timer files."""

    def __init__(self, crontab: str, description: str) -> None:
        """Enables space for crontab data."""
        crontab_pattern = re.compile(
            r"""
            ^                              # start of line
            (\*|[0-9]{1,2}|[1-5][0-9]|60)  # minute (0-59)
            \s+                            # whitespace separator
            (\*|[0-9]{1,2}|[1-5][0-9]|60)  # hour (0-23)
            \s+                            # whitespace separator
            (\*|[1-9]|[1-2][0-9]|3[0-1])   # day of month (1-31)
            \s+                            # whitespace separator
            (\*|[1-9]|1[0-2])              # month (1-12)
            \s+                            # whitespace separator
            (\*|[0-6])                     # day of week (0-6, where 0 = Sunday)
            \s+                            # whitespace separator
            (.*)                           # command
            $                              # end of line
            """,
            re.VERBOSE,
        )
        if crontab_pattern.match(crontab):
            self._crontab = crontab  # Store the crontab in the object.
            self._description = description  # Stores the description.
            self._process_crontab()  # Process the crontab data to update the
            # object.
        else:
            raise ValueError("Invalid cron line.")

    # Read-only properties for accessing internal state
    @property
    def minute(self):
        return self._minute

    @property
    def hour(self):
        return self._hour

    @property
    def daymonth(self):
        return self._daymonth

    @property
    def month(self):
        return self._month

    @property
    def dayweek(self):
        return self._dayweek

    @property
    def command(self):
        return self._command

    # Interface

    def __str__(self) -> str:
        return self.generate_crontab()

    def generate_crontab(self) -> str:
        """Returns the stored crontab line from the object's stored details."""
        return self._crontab

    def generate_service(self, name: str) -> str:
        """Generates a systemd service file from the object's stored
        details.

        """
        working_directory = os.path.expanduser("~") + "/"

        result = f"""
        [Unit]
        Description={self._description}
        Wants={name}.timer

        [Service]
        ExecStart={self._command}
        WorkingDirectory={working_directory}
        StandardOutput=syslog
        StandardError=syslog

        [Install]
        WantedBy=multi-user.target
        """

        return textwrap.dedent(result[1:])

    def generate_timer(self, name: str) -> str:
        """Generates a systemd timer file from the object's stored details."""

        month = f"{self._month:02d}" if self._month != "*" else "*"
        daymonth = f"{self._daymonth:02d}" if self._daymonth != "*" else "*"
        hour = f"{self._hour:02d}" if self._hour != "*" else "*"
        minute = f"{self._minute:02d}" if self._minute != "*" else "*"

        result = f"""
        [Unit]
        Description={self._description}
        Requires={name}.service

        [Timer]
        Unit={name}.service
        OnCalendar={self._dayweek} *-{month}-{daymonth} {hour}:{minute}:00
        Persistent=true

        [Install]
        WantedBy=timers.target
        """

        return textwrap.dedent(result[1:])

    # Logic

    def _process_crontab(self) -> None:
        """Extracts all details from the imported crontab string."""
        crontab_elements = self._crontab.split()
        (
            minute,
            hour,
            daymonth,
            month,
            dayweek,
            *command_and_args,
        ) = crontab_elements

        # Convert day of week to systemd format
        day_week_map = {
            "0": "Sun",
            "1": "Mon",
            "2": "Tue",
            "3": "Wed",
            "4": "Thu",
            "5": "Fri",
            "6": "Sat",
        }

        self._minute = "*" if minute == "*" else int(minute)
        self._hour = "*" if hour == "*" else int(hour)
        self._daymonth = "*" if daymonth == "*" else int(daymonth)
        self._month = "*" if month == "*" else int(month)
        self._dayweek = "*" if dayweek == "*" else day_week_map[dayweek]
        self._command = " ".join(command_and_args)


if __name__ == "__main__":
    test_crontab = Crontab(
        "0 12 * * 6 ls -la /some/location/to/look", "A test description"
    )

    print("--- Crontab Entry:\n")
    print(test_crontab.generate_crontab() + "\n")
    print("--- Systemd Service File:\n")
    print(test_crontab.generate_service("looking"))
    print("--- Systemd Timer File:\n")
    print(test_crontab.generate_timer("looking"))
    print("---\n")
