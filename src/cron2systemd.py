#!/usr/bin/env python3

"""cron2system.py: A utility that converts a crontab string into service
and timer files for systemd use.

Copyright © 2024 Lampros Liontos

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see https://www.gnu.org/licenses/.

"""

import sys
import crontab
import arguments
import logging

logger = logging.getLogger("cron2systemd")
handler = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)


def set_log(level_string: str):
    """Create a logging stream, and set the default level."""
    log_levels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
    }

    level = log_levels.get(level_string.lower(), logging.INFO)

    # logging.basicConfig(level=logLevels[level_string])
    logger.setLevel(level)
    logger.info(f"Logging level set to {level_string.upper()}")

    # return logger


def file_write(filename: str, content: str) -> None:
    """Write the content to the specified filename."""
    try:
        with open(filename, "w") as file_space:
            file_space.write(content)
        logger.info(f"Successfully wrote to {filename}")
    except IOError as e:
        logger.error(f"Failed to write to {filename}: {e}")


def main():
    args = arguments.parser()

    set_log(args.logging)

    try:
        cron_object = crontab.Crontab(args.crontab, args.description)

        logger.debug("Crontab: " + str(cron_object))

        logger.debug("Generating service and timer contents.")
        service_content = cron_object.generate_service(args.name)
        timer_content = cron_object.generate_timer(args.name)

        service_file = f"{args.name}.service"
        timer_file = f"{args.name}.timer"

        logger.info(f"Generating {service_file}...")
        logger.debug(service_content)
        file_write(service_file, service_content)

        logger.info(f"Generating {timer_file}...")
        logger.debug(timer_content)
        file_write(timer_file, timer_content)

        logger.info("Complete!")
        if args.description == "A converted cron job.":
            logger.warning(
                "You did not include a description for the service.  "
                "You might want to edit the service and timer files "
                "to update the description before deployment."
            )

    except ValueError as e:
        logger.error("Failed to parse cron entry: %s", e)
        sys.exit(1)

    except Exception as e:
        logger.error("An unexpected error occurred: %s", e)
        sys.exit(1)


if __name__ == "__main__":
    main()
