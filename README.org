#+TITLE: cron2systemd

~cron2systemd~ is a Python utility that converts a crontab entry into ~systemd~
service and timer files, which you can then place in your user ~systemd~
directory.  This tool will help you migrate your ~cron~ jobs to work with
~systemd~ systems.

* Installation

** Prerequisites

   This utility requires Python 3.6 or higher.  Since this is specific to
   ~systemd~ (and ~cron~), you will probably want to use this in a Linux environment.

** Steps

   1. Clone the repository and enter the directory:

      #+begin_src bash
        git clone https://gitlab.com/reteo/cron2systemd.git
        cd cron2systemd
      #+end_src

   2. (Optional) Create and activate a virtual environment:

      #+begin_src bash
        python3 -m venv venv
        source venv/bin/activate
      #+end_src

   3. Make sure the ~cron2systemd.py~ script has the proper permissions:

      #+begin_src bash
        chmod +x src/cron2systemd.py
      #+end_src

      This ensures that the program can be executed.

   4. Place a symbolic link to the ~cron2systemd.py~ executable inside your
      ~PATH~.

      #+begin_src bash
        cd ~/.local/bin
        ln -s /path/to/location/src/cron2systemd.py .
      #+end_src

* Usage

  To use ~crontsystemd~, run the script with the necessary arguments:

  #+begin_src bash
    cron2systemd.py "crontab entry" "service name" [options]
  #+end_src

** Example

   For an example, suppose we want to convert a crontab entry that pings
   Google every hour (suppose you want to track your connectivity?) into
   something that runs under ~systemd~?

   #+begin_src bash
     cron2systemd.py "00 * * * * ping -c 1 google.com" google_ping
   #+end_src

   You will find that two new files will be written in the current
   directory:
   
*** ~google_ping.service~
    #+caption: google_ping.service
    #+begin_example
      [Unit]
      Description=A converted cron job.
      Wants=google_ping.timer

      [Service]
      ExecStart=ping -c 1 google.com
      WorkingDirectory=/home/reteo/
      StandardOutput=syslog
      StandardError=syslog

      [Install]
      WantedBy=multi-user.target
    #+end_example

*** ~google_ping.timer~
    #+caption: google_ping.timer
    #+begin_example
      [Unit]
      Description=A converted cron job.
      Requires=google_ping.service

      [Timer]
      Unit=google_ping.service
      OnCalendar=* *-*-* *:00:00
      Persistent=true

      [Install]
      WantedBy=timers.target
    #+end_example

    If these are good enough, you can move them to your ~systemd~ user
    directory, and then enable and start it as normal:

    #+begin_src bash
      mv google_ping.timer google_ping.service ~/.config/systemd/user/
      systemctl --user enable google_ping.timer && systemctl --user start google_ping.timer
    #+end_src

* Arguments

  This script accepts the following arguments:

  - ~crontab~ :: (Required) You enter your crontab entry here, including the command
    after the times.
  - ~name~ :: (Required) This is the name you want to give your service.  This will be
    used in generating the service filenames.
  - ~-d~ or ~--description~ :: (Optional) This is the description for your service; this
    will be what ~systemd~ announces when starting the timer. 
  - ~-l~ or ~--logging~ :: (Optional) This is the logging level for your script.  By
    default, it is set to ~info~, but you can choose between ~debug~ (show all
    logs), ~info~, ~warning~ (only show warnings and errors) and ~error~ (only
    show errors).
    
** Full Command-Line Example

   #+begin_src bash
     cron2systemd.py "00 * * * * ping -c 1 google.com" google_ping -d "Check to see if Google can be reached." -l "debug"
   #+end_src

   This will give you a lot of information as the file is being generated:

   #+begin_example
     2024-06-12 14:35:47,869 - cron2systemd - INFO - Logging level set to DEBUG
     2024-06-12 14:35:47,870 - cron2systemd - DEBUG - Crontab: 00 * * * * ping -c 1 google.com
     2024-06-12 14:35:47,870 - cron2systemd - DEBUG - Generating service and timer contents.
     2024-06-12 14:35:47,870 - cron2systemd - INFO - Generating google_ping.service...
     2024-06-12 14:35:47,870 - cron2systemd - DEBUG - [Unit]
     Description=Check to see if Google can be reached.
     Wants=google_ping.timer

     [Service]
     ExecStart=ping -c 1 google.com
     WorkingDirectory=/home/reteo/
     StandardOutput=syslog
     StandardError=syslog

     [Install]
     WantedBy=multi-user.target

     2024-06-12 14:35:47,870 - cron2systemd - INFO - Successfully wrote to google_ping.service
     2024-06-12 14:35:47,870 - cron2systemd - INFO - Generating google_ping.timer...
     2024-06-12 14:35:47,870 - cron2systemd - DEBUG - [Unit]
     Description=Check to see if Google can be reached.
     Requires=google_ping.service

     [Timer]
     Unit=google_ping.service
     OnCalendar=* *-*-* *:00:00
     Persistent=true

     [Install]
     WantedBy=timers.target

     2024-06-12 14:35:47,871 - cron2systemd - INFO - Successfully wrote to google_ping.timer
     2024-06-12 14:35:47,871 - cron2systemd - INFO - Complete!
   #+end_example
   
* Code Structure

  - ~cron2systemd.py~ :: The main script that processes the crontab entry and
    generates ~systemd~ files.
  - ~arguments.py~ :: Defines the command-line argument parser.
  - ~crontab.py~ :: Contains the ~Crontab~ class that stores a crontab entry and
    converts it into ~systemd~ service and timer content.

** The ~Crontab~ Class

   This class is an immutable container object that is assigned a crontab
   line upon creation.  You can either return the original crontab using a
   ~str()~ typecast, or generate ~systemd~ content using the ~generate_service()~
   and ~generate_timer()~ methods.  You can also get the various stored values
   using the ~minute~, ~hour~, ~daymonth~, ~month~, ~dayweek~, and ~command~ properties.

* Contributing

  Contributions are generally welcome. Please follow these steps to
  contribute:

  - Fork the repository.
  - Create a new branch for your feature or bugfix.
  - Make your changes.
  - Submit a merge request (MR) with a detailed description of your changes.

* License

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along
  with this program.  If not, see <https://www.gnu.org/licenses/>.
